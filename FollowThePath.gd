extends "res://NPC.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var audio_vision_worker = $ASP_Vision_Worker
onready var audio_walking_worker = $ASP_Walking_Worker

export var startPoint :int = 0
var point:int
var pointPos:Vector2
var path:Curve2D
var speed = 500

# Called when the node enters the scene tree for the first time.
func _ready():
	audio_vision_worker.volume_db = -80
	audio_walking_worker.volume_db = -80
	path = get_parent().curve
	if startPoint <= path.get_point_count() :
		point = startPoint
	else : 
		point = path.get_point_count()
	pointPos = path.get_point_position(point)
	position = pointPos
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if position.distance_to(pointPos) < 50:
		position = pointPos
		pointPos = _get_next_point()
	else :
		look_at(pointPos)
		rotate(PI/2)
		var direction :Vector2 
		direction = position.direction_to(pointPos)
		linear_velocity = direction * speed * delta
	pass

func _get_next_point():
	if point < path.get_point_count() - 1 :
		point += 1
	else : 
		point = 0
	return path.get_point_position(point)

func _player_is_seen():
	audio_vision_worker.volume_db = 0
	
func _player_is_unseen():
	audio_vision_worker.volume_db = -80
