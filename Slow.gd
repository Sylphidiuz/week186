extends "res://Children.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var slow:float = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	var grow: Timer = Timer.new()
	grow.name = "slow"
	add_child(grow)
	grow.connect("timeout", self, "slow")
	grow.wait_time = 0.5
	grow.start()

func slow():
	#print_debug($Sprite.transform.get_scale())
	speed -= slow

func _physics_process(delta):
	if step == 0:
		._physics_process(delta)
	else :
		alternate_physics(delta)

func alternate_physics(delta):
	var direction :Vector2 
	# the distance of the target
	var dist = 0 
	if target != null :
		dist = target.position.distance_to(self.position)
		direction = position.direction_to(target.position)
		if direction.x != 1:
				rotation = position.angle_to_point(target.position) - PI/2
	
	if dist > 100 :
		linear_velocity = direction * speed * delta
	else :
		linear_velocity = Vector2(0,0)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
