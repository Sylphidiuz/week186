extends RigidBody2D


var target : Node2D


#onready var audio_vision_npc = $ASP_Vision_NPC

var myself = self

signal player_seen
signal player_unseen
#export var speed = 12000


# Called when the node enters the scene tree for the first time.
func _ready():
	$SightCone.connect("body_entered",self,"_sight_entered")
	$SightCone.connect("body_exited",self,"_sight_quited")
	$SightCone.collision_mask = 3
	#myself.connect("player_seen",self,"_player_is_seen")
	#myself.connect("player_unseen",self,"_player_is_unseen")

func _process(delta):
	pass
	
		

#func _player_is_seen():
	#audio_vision.stream_paused = false
	
#func _player_is_unseen():
#	audio_vision.stream_paused = true

#This function is called when the player or a NPC is in sights
#It must be called in the Scripts which inherit from this one with "._sight_entered"
#Otherwise, the Node will have to check if the body in sight isn't itself
func _sight_entered(body : Node):
	#print_debug(name)
	if body == self :
		return false
	if body.get_groups()[0] == "player":
		_player_is_seen()
		#audio_vision.stream_paused = false
		body.looked()
	return true
	
func _sight_quited(body : Node):
	if body == self :
		return false
	if body.get_groups()[0] == "player":
		_player_is_unseen()
		#audio_vision.stream_paused = true
		body.unlooked()
		
	return true

func _player_is_seen():
	pass
	
func _player_is_unseen():
	pass
	
func gone():
	pass
