extends "res://Children.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var gaze:Node2D
export var interest: int = 4
export var flickerTime:float = 5
var grow: Timer

# Called when the node enters the scene tree for the first time.
func _ready():
	grow = Timer.new()
	grow.name = "slow"
	add_child(grow)
	grow.connect("timeout", self, "flick")
	grow.wait_time = flickerTime
	grow.one_shot = true
	grow.start()
	$SightCone/Aura.connect("body_entered",self,"look_at_this")

func flick():
	#print_debug(gaze)
	gaze = null

func look_at_this(body:Node):
	if gaze == null && body.get_groups().find("children") == -1 && body.get_groups().find("NPC") != -1:
		#print_debug("trigger")
		gaze = body
		if interest != 0:
			interest -= 1
			grow.start()
		else :
			interest -= 1
			if target != null && target.get_groups()[0] == "player" :
				target.people.remove(target.people.find(self))
				target.folowers -= 1
				target = body
		

func _physics_process(delta):
	if step == 0:
		._physics_process(delta)
	else:
		heritage = true
		alternate_physics(delta)

func alternate_physics(delta):
	var direction :Vector2 
	# the distance of the target
	var dist = 0 
	if target != null :
		dist = target.position.distance_to(self.position)
		direction = position.direction_to(target.position)
		if gaze == null :
			rotation = position.angle_to_point(target.position) - PI/2
		else:
			rotation = position.angle_to_point(gaze.position) - PI/2
			
	
	if dist > 100 :
		linear_velocity = direction * speed * delta
	else :
		linear_velocity = Vector2(0,0)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
