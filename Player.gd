extends RigidBody2D
signal dead
var rng = RandomNumberGenerator.new()
var velocity: Vector2
export var speed : int = 160
export var seen: int = 0
var timerTicked: bool = false
var tail:Node
var people = []
var folowers:int = 0

onready var audio_walking = $AudioStreamPlayer
onready var end_fade_1 = $End_Fade_Sprite_1
onready var end_fade_2 = $End_Fade_Sprite_2
onready var end_timer_1 = $End_Timer_1
onready var end_timer_2 = $End_Timer_2
onready var tween_sprite = $Tween_Sprite_Fade
onready var tween_sprite_2 = $Tween_Sprite_Fade2
onready var audio_noise = $ASP_Noise

var end_game = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func stopFriendship():
	var pos = rng.randi_range(0,2)
	if people[pos] != null :
		people[pos].target = null
	people.remove(pos)
	folowers -= 1

func looked():
	#print_debug("looked")
	seen += 1
	#tween_audio.interpolate_property(audio_noise, "volume_db", -3, -80, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	audio_noise.stream_paused = true
	
func unlooked():
	seen -= 1
	#tween_audio.interpolate_property(audio_noise, "volume_db", -80, -3, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	
	
# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print(master_bus_volume)
	if !people.empty() && people.size() > 3 :
		stopFriendship()
	if seen == 0 && !timerTicked:
		$FadeTimer.start()
		timerTicked = true
		audio_noise.stream_paused = false
	velocity = Vector2(0,0)
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		audio_walking.stream_paused = false
	elif velocity.length() <= 0:
		audio_walking.stream_paused = true

func _physics_process(delta):
	rotation = 0
	linear_velocity = velocity * delta* speed


func _on_FadeTimer_timeout():
	if seen == 0:
		$Sprite.modulate.a -= 0.1
		if $Sprite.modulate.a <= 0 and end_game == false:
			audio_noise.stream_paused = true
			emit_signal("dead") 
	else :
		$Sprite.modulate.a += 0.1
		if $Sprite.modulate.a >= 1 :
			$Sprite.modulate.a =1
			$FadeTimer.stop()
			timerTicked = false


func _on_End_Fade_1_body_entered(body):
	end_game = true
	end_timer_1.start()
	
	#start tween
	#start timer
	#once timer has ended, tween volume of master bus down to -80, start another tween for fade to black,
	#and start another timer to match the tween with extra
	#once that timer has ended, display "END, please close the window and have a good day"
	

func _on_End_Timer_1_timeout():
	print ("Timer1 has timed out")
	end_fade_1.visible = true
	tween_sprite.interpolate_property(end_fade_1, "modulate", 
	Color(1, 1, 0, 0), Color(1, 1, 0, 1), 12.0, 
	Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween_sprite.start()
	
func _on_Tween_Sprite_Fade_tween_completed(object, key):
	end_timer_2.start()
	
	
func _on_End_Timer_2_timeout():
	print("Timer2 has timed out")
	AudioServer.set_bus_volume_db(0, -80)
	end_fade_2.visible = true
	tween_sprite_2.interpolate_property(end_fade_2, "modulate",
	Color(0, 0, 0, 0), Color(0, 0, 0, 1), 5.0,
	Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween_sprite_2.start()

func _on_Tween_Sprite_Fade2_tween_completed(object, key):
	set_physics_process(false)
	set_process(false)
	get_tree().change_scene("res://End_Game_Message.tscn")






