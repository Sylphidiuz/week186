extends "res://NPC.gd"

var heritage:bool = false
var step:int = 0
var head:Node
var tail:Node
var folowers:int = 0
var people = []
var rng = RandomNumberGenerator.new()
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed = 8200

onready var audio_vision_children = $ASP_Vision_Children
onready var audio_walking_children = $ASP_Walking_Children


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("children")
	var timer: Timer = Timer.new()
	timer.name = "Change"
	add_child(timer)
	timer.connect("timeout", self, "_on_Timer_timeout")
	timer.wait_time = 2
	timer.start()
	rng.randomize()
	audio_vision_children.volume_db = -80
	audio_walking_children.volume_db = -80

func _on_Timer_timeout():
	if target == null :
		rotation += PI
		if(rng.randi_range(0,1) == 0) :
			rotation += PI/2*rng.randf_range(0,0.5)
		else:
			rotation -= PI/2*rng.randf_range(0,0.5)
#The operatiosn on the rigidbody are made here
func _physics_process(delta):
	if heritage:
		return
	#the direction to look at the target
	var direction :Vector2 
	# the distance of the target
	var dist = 0 
	if target != null :
		dist = target.position.distance_to(self.position)
		direction = position.direction_to(target.position)
		if direction.x != 1:
				rotation = position.angle_to_point(target.position) - PI/2
	else :
		direction = position.direction_to($Forward.get_global_position())
		dist = 101
	if dist > 100 :
		linear_velocity = direction * speed * delta
		audio_walking_children.volume_db = 0
	else :
		linear_velocity = Vector2(0,0)
		audio_walking_children.volume_db = -80

func alternate_physics(delta):
	pass
	
func alternate_process(delta):
	pass

func alternate_sight_entered():
	pass
	

func gone():
	if target != null :
		if target.people.find(self) != -1:
			target.people.remove(target.people.find(self))
		target =null

func atachTo(body:Node):
	body.people += [self]
	body.people += people
	for n in range(folowers):
		people[n].target = body
	body.folowers += 1 + folowers
	folowers = 0
	target = body

func stopFriendship():
	var pos = rng.randi_range(0,2)
	if people[pos] != null :
		people[pos].target = null
	people.remove(pos)
	folowers -= 1
	



func _sight_entered(body : Node):
	if ._sight_entered(body) :
			if target == null :
				if body.get_groups().find("player") != -1 || (body.get_groups().find("children") != -1 && body.target == null):
					atachTo(body)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if people.size() > 3:
		stopFriendship()


func _player_is_seen():
	audio_vision_children.volume_db = 0


func _player_is_unseen():
	audio_vision_children.volume_db = -80
