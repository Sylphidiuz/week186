extends RigidBody2D

export var speed: int = 80
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	rotation = 0
	linear_velocity = Vector2(0,-speed)
	pass




func _on_Area2D_body_entered(body: Node):
	if body.get_groups().find("NPC") != -1 :
		body.gone()
		body.queue_free()
	pass # Replace with function body.
