extends "res://NPC.gd"

#Each time a rigidbody enters the Follower's sight, it follows it. 
#This script is more an exemple of inheritence on NPC than a real behavioir

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed = 7000

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

#The operatiosn on the rigidbody are made here
func _physics_process(delta):
	#the direction to look at the target
	var direction :Vector2 
	# the distance of the target
	var dist = 0 
	if target != null :
		dist = target.position.distance_to(self.position)
		direction = position.direction_to(target.position)
		if direction.x != 1:
				rotation = position.angle_to_point(target.position) - PI/2
			
	if dist > 100 :
		linear_velocity = direction * speed * delta
	else :
		linear_velocity = Vector2(0,0)

func _process(delta):
	pass
		
 #How to behave when a rigidbody enter your sight
func _sight_entered(body : Node):
	#testing base _sight_entered wich prevent the Node to detect itself
	if ._sight_entered(body) :
		target = body


func _on_NPC_player_seen():
	pass # Replace with function body.


func _on_NPC_player_unseen():
	pass # Replace with function body.
