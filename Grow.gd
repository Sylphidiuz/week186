extends "res://Children.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var maxScale:float = 0.16
export var growth:float = 0.01
func _physics_process(delta):
	if step == 0:
		._physics_process(delta)
	else :
		alternate_physics(delta)

func alternate_physics(delta):
	var direction :Vector2 
	# the distance of the target
	var dist = 0 
	if target != null :
		dist = target.position.distance_to(self.position)
		direction = position.direction_to(target.position)
		if direction.x != 1:
				rotation = position.angle_to_point(target.position) - PI/2
	
	if dist > 100 :
		linear_velocity = direction * speed * delta
	else :
		linear_velocity = Vector2(0,0)
	
func alternate_process(delta):
	pass
	

func alternate_sight_entered():
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	var grow: Timer = Timer.new()
	grow.name = "grow"
	add_child(grow)
	grow.connect("timeout", self, "_grow")
	grow.wait_time = 1.8
	grow.start()

func _grow():
	if step !=0 :
		if $Sprite.scale.x < maxScale : 
			$Sprite.scale += Vector2(growth,growth)
			$CollisionShape2D.shape.radius += 400 * growth


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
