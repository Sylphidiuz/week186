extends "res://NPC.gd"

export var speed = 6200
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var audio_vision_parent = $ASP_Vision_Parent
onready var audio_walking_parent = $ASP_Walking_Parent


# Called when the node enters the scene tree for the first time.
func _ready():
	target = get_tree().get_nodes_in_group("player")[0]
	audio_vision_parent.volume_db = -80
	audio_walking_parent.volume_db = -80


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	#the direction to look at the target
	var direction :Vector2 
	# the distance of the target
	var dist = 0 
	if target != null :
		dist = target.position.distance_to(self.position)
		direction = position.direction_to(target.position)
		if direction.x != 1:
				rotation = position.angle_to_point(target.position) - PI/2
			
	if dist > 100 :
		linear_velocity = direction * speed * delta
		audio_walking_parent.volume_db = 0
	else :
		linear_velocity = Vector2(0,0)
		audio_walking_parent.volume_db = -80

func _player_is_seen():
	audio_vision_parent.volume_db = 0
	
func _player_is_unseen():
	audio_vision_parent.volume_db = -80
